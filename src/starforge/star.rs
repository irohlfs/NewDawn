use std::error::Error;
use starforge::util::types::*;
use starforge::util::dice::Dice;



#[derive(PartialEq, Clone, Copy, Debug)]
pub enum StarType {
    SuperGiant,
    Giant,
    NeutronStar,
    BackHole,
    MainSequence,
    WhiteDwarf,
}

#[derive(Debug,Clone,Copy)]
pub enum SpectralClass {
    O,
    B,
    A,
    F,
    G,
    K,
    M,
    NS,
    BH,
}

#[derive(Debug)]
pub struct Star {
    star_type: StarType,
    spectral_class: SpectralClass,
    spectral_subclass: u8,
}


impl Star {
    pub fn new (dice: &mut Dice) -> BoxResult<Star> {
        let d100 = dice.role(100,3).clone();
        let d10 = dice.role(10,1).clone();
        let star = match d100[0] {
            1 => match d100[1] {
                1 => match d100[2] {
                    1...10 => {
                        let spectral_subclass = d10[0] - 1;
                        Star {
                            star_type: StarType::SuperGiant,
                            spectral_class: SpectralClass::B,
                            spectral_subclass,
                        }
                    }
                    11...20 => {
                        let spectral_subclass = d10[0] - 1;
                        Star {
                            star_type: StarType::SuperGiant,
                            spectral_class: SpectralClass::A,
                            spectral_subclass,
                        }
                    }
                    21...40 => {
                        let spectral_subclass = d10[0] - 1;
                        Star {
                            star_type: StarType::SuperGiant,
                            spectral_class: SpectralClass::F,
                            spectral_subclass,
                        }
                    }
                    41...60 => {
                        let spectral_subclass = d10[0] - 1;
                        Star {
                            star_type: StarType::SuperGiant,
                            spectral_class: SpectralClass::G,
                            spectral_subclass,
                        }
                    }
                    61...80 => {
                        let spectral_subclass = d10[0] - 1;
                        Star {
                            star_type: StarType::SuperGiant,
                            spectral_class: SpectralClass::K,
                            spectral_subclass,
                        }
                    }
                    81...100 => {
                        let spectral_subclass = d10[0] - 1;
                        Star {
                            star_type: StarType::SuperGiant,
                            spectral_class: SpectralClass::K,
                            spectral_subclass,
                        }
                    }
                    _ => bail!("Invalid Number"),
                }
                2...5 => {
                    let spectral_subclass = d10[0] - 1;
                    Star {
                        star_type: StarType::Giant,
                        spectral_class: SpectralClass::F,
                        spectral_subclass,
                    }
                }
                6...10 => {
                    let spectral_subclass = d10[0] - 1;
                    Star {
                        star_type: StarType::Giant,
                        spectral_class: SpectralClass::G,
                        spectral_subclass,
                    }
                }
                11...55 => {
                    let spectral_subclass = d10[0] - 1;
                    Star {
                        star_type: StarType::Giant,
                        spectral_class: SpectralClass::K,
                        spectral_subclass,
                    }
                }
                56...95 => {
                    let spectral_subclass = d10[0] - 1;
                    Star {
                        star_type: StarType::Giant,
                        spectral_class: SpectralClass::M,
                        spectral_subclass,
                    }
                }
                96...99 => {
                    Star {
                        star_type: StarType::NeutronStar,
                        spectral_class: SpectralClass::NS,
                        spectral_subclass: 0,
                    }
                }
                100 => {
                    match d100[2] {
                        1...20 => {
                            Star {
                                star_type: StarType::BackHole,
                                spectral_class: SpectralClass::BH,
                                spectral_subclass: 0,
                            }
                        }
                        21...100 => {
                            let spectral_subclass = (d10[0] / 2) + 4;
                            Star {
                                star_type: StarType::MainSequence,
                                spectral_class: SpectralClass::O,
                                spectral_subclass,
                            }
                        }
                        _ => bail!("Invalid Number Supplied")
                    }
                }
                _ => bail!("Invalid Number Supplied"),
            }
            2 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::MainSequence,
                    spectral_class: SpectralClass::B,
                    spectral_subclass,
                }
            }
            3...4 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::MainSequence,
                    spectral_class: SpectralClass::A,
                    spectral_subclass,
                }
            }
            5...8 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::MainSequence,
                    spectral_class: SpectralClass::F,
                    spectral_subclass,
                }
            }
            9...15 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::MainSequence,
                    spectral_class: SpectralClass::G,
                    spectral_subclass,
                }
            }
            16...30 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::MainSequence,
                    spectral_class: SpectralClass::K,
                    spectral_subclass,
                }
            }
            31...93 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::MainSequence,
                    spectral_class: SpectralClass::M,
                    spectral_subclass,
                }
            }
            94 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::WhiteDwarf,
                    spectral_class: SpectralClass::B,
                    spectral_subclass,
                }
            }
            95...96 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::WhiteDwarf,
                    spectral_class: SpectralClass::A,
                    spectral_subclass,
                }
            }
            97...98 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::WhiteDwarf,
                    spectral_class: SpectralClass::F,
                    spectral_subclass,
                }
            }
            99 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::WhiteDwarf,
                    spectral_class: SpectralClass::G,
                    spectral_subclass,
                }
            }
            100 => {
                let spectral_subclass = d10[0] - 1;
                Star {
                    star_type: StarType::WhiteDwarf,
                    spectral_class: SpectralClass::K,
                    spectral_subclass,
                }
            }
            _ => bail!("Invalid Number Supplied"),
        };
        Ok(star)
    }

    /// Gets the number of Planets present in the system
    pub fn number_of_planets(&mut self,dice: &mut Dice) -> u8 {
        let mut planet_number:u8 = 0;
        let d100 = dice.role(100,1);
        match self.star_type {
            StarType::SuperGiant | StarType::Giant => {
                match d100[0] {
                    50...61 => {
                        planet_number = dice.role(6,1)[0];
                    }
                    _ => {
                        // Nothing
                    }
                }
            }
            StarType::MainSequence => {
                match self.spectral_class {
                    SpectralClass::O | SpectralClass::B => {
                        match d100[0] {
                            50...61 => {
                                planet_number = dice.role(10,1)[0];
                            }
                            _ => {
                                // nothing
                            }
                        }
                    }
                    SpectralClass::A => {
                        if (d100[0] % 2) == 0 {
                            planet_number = dice.role(10,1)[0];
                        }
                    }
                    SpectralClass::F | SpectralClass::G => {
                        match d100[0] {
                            1...99 => {
                                let d6 = dice.role(6,2);
                                planet_number = d6[0] + d6[1] + 3;
                            }
                            _ => {
                                //nothing
                            }
                        }
                    }
                    SpectralClass::K => {
                        match d100[0] {
                            1...99 => {
                                let d6 = dice.role(6,2);
                                planet_number = d6[0] + d6[1];
                            }
                            _ => {
                                //nothing
                            }
                        }
                    }
                    SpectralClass::M => {
                        if (d100[0] % 2) == 0 {
                            planet_number = dice.role(6,1)[0];
                        }
                    }
                    _ => {
                        // Nothing
                    }
                }
            }
            StarType::WhiteDwarf => {
                match d100[0] {
                    50...61 => {
                        planet_number = dice.role(6,1)[0] / 2;
                    }
                    _ => {
                        // nothing
                    }
                }
            }
            StarType::NeutronStar | StarType::BackHole => {
                match d100[0] {
                    50...56 => {
                        planet_number = dice.role(6,1)[0] / 2;
                    }
                    _ => {
                        // nothing
                    }
                }
            }
        }
        planet_number
    }

/// GETTER Functions

    /// Get Startype
    pub fn get_startype(&self) -> StarType {
        self.star_type.clone()
    }

    /// Get Spectralclass
    pub fn get_spectralclass(&self) -> SpectralClass {
        self.spectral_class.clone()
    }

}


#[test]
fn test_starclass_generate() {
    let mut dice = Dice::new([0;32]);
    let star = Star::new(&mut dice).unwrap();
    let result = match star.star_type {
        StarType::MainSequence => true,
        _ => false,
    };
    assert_eq!(true, result)
}

#[test]
fn test_number_of_stars() {
    let mut dice = Dice::new([0;32]);
    let mut star = Star::new(&mut dice).unwrap();
    let result = star.number_of_planets(&mut dice);
    assert_eq!(0,result);
}
