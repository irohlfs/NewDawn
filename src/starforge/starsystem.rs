

use starforge::util::coordinates::*;
use starforge::star::*;
use starforge::planet::*;
use starforge::util::types::*;
use starforge::util::dice::*;
use std::error::Error;
pub struct StarSystem {
    position: Coordinates,
    stars: Vec<Star>,
    planets: Vec<Planet>,
}



impl StarSystem {



    ///Generates an empty starsystem at given position
    pub fn new(position: Coordinates) -> StarSystem {
        StarSystem {
            position,
            stars: Vec::new(),
            planets: Vec::new(),
        }
    }

    /// Determines the number of Stars in the System.
    pub fn number_of_stars(dice: &mut Dice) -> BoxResult<u8> {
        let d100 = dice.role(100,1).clone();
        match d100[0] {
            1...40 => Ok(1),
            41...90 => Ok(2),
            91...99 => Ok(3),
            100 => Ok(4),
            _ => bail!("Invalid Number"),
        }
    }

    pub fn planets_in_zones(&self,planets: u8) -> [u8;3] {
        let mut zones = [0;3];
        let total_planets = planets.clone();
        if self.stars.first().unwrap().get_startype() == StarType::WhiteDwarf ||
            self.stars.first().unwrap().get_startype() == StarType::NeutronStar ||
            self.stars.first().unwrap().get_startype() == StarType::BackHole {
            zones[2] = total_planets;
        } else {
            match planets {
                1...3 => {
                    zones[0] = 0;
                    zones[1] = 1;
                    zones[2] = total_planets - 1;
                }
                4...5 => {
                    zones[0] = 1;
                    zones[1] = 2;
                    zones[2] = total_planets - 3;
                }
                6 => {
                    zones[0] = 1;
                    zones[1] = 2;
                    zones[2] = total_planets - 3;
                }
                7 => {
                    zones[0] = 1;
                    zones[1] = 3;
                    zones[2] = total_planets - 4;
                }
                _ => {
                    zones[0] = 2;
                    zones[1] = 4;
                    zones[2] = total_planets - 6;
                }
            }
        }
        zones
    }
}




#[test]
fn test_number_of_stars() {
    let mut dice = Dice::new([0;32]);
    let stars = StarSystem::number_of_stars(&mut dice).unwrap();
    assert_eq!(2,stars);
}

