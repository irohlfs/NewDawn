pub struct Coordinates {
    x: i8,
    y: i8,
    z: i8,
}

impl Coordinates {
    /// Constructor
    pub fn new(x: i8,y:i8,z:i8) -> Coordinates {
        Coordinates {
            x,
            y,
            z,
        }
    }

    /// get_x
    pub fn get_x(&self) -> i8 {
        self.x
    }

    /// get_y
    pub fn get_y(&self) -> i8 {
        self.y
    }

    /// get_z
    pub fn get_z(&self) -> i8 {
        self.z
    }


}



