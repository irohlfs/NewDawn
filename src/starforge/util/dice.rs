
use rand::*;
pub struct Dice {
        seed:[u8;32],
        rng: StdRng,
}

impl Dice {

    pub fn create_seed () -> [u8;32] {
        let mut seed = [0;32];
        for n in 0 .. 32 {
         let seedn:u8   = random();
         seed[n] = seedn as u8;
        }
        seed
    }

    pub fn new(seed:[u8;32]) -> Dice {
        let rng: StdRng = SeedableRng::from_seed(seed);
        Dice {
            seed,
            rng,
        }
    }

    pub fn role(&mut self,dicetype:u8,amount:u8) -> Vec<u8> {
       let mut values = Vec::new();
       for _d in 0..amount {
           values.push(self.rng.gen_range(0,dicetype)+1);
       }
        values
    }
}
