use starforge::util::dice::Dice;

#[derive(Debug)]
enum PlanetZone {
    Hot,
    Life,
    Cold,
}

#[derive(Debug)]
enum PlanetType {
    Asteroids,
    Giant,
    Rock,
    Cold,
    Desert,
    Hostile,
    Marginal,
    Earthlike,
}

#[derive(Debug)]
pub struct Planet {
    zone: PlanetZone,
    planet_type:  PlanetType,
    diameter: f64,
    moons: f64,
    temperature: f64,
    minerals: f64,
}

impl Planet {
    pub fn new() {

    }
}
