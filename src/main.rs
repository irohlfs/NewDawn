extern crate rand;

#[macro_use]
extern crate simple_error;


mod starforge;

use starforge::util::dice;
use starforge::star::*;
fn main() {

    println!("Initialize Game Board");
    let seed = dice::Dice::create_seed();
    let mut dice = dice::Dice::new(seed);
    /// let mut dice = dice::Dice::new([0;32]);
   println!("{:?}",starforge::starsystem::StarSystem::number_of_stars(&mut dice));
    let mut star = Star::new(&mut dice).unwrap();
    println!("{:?}",star);
    println!("{:?}",star.number_of_planets(&mut dice));

    }




